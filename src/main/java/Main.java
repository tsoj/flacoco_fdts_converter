import fr.spoonlabs.flacoco.api.Flacoco;
import fr.spoonlabs.flacoco.api.result.FlacocoResult;
import fr.spoonlabs.flacoco.api.result.Location;
import fr.spoonlabs.flacoco.core.config.FlacocoConfig;
import fr.spoonlabs.flacoco.core.test.method.TestMethod;
import spoon.reflect.code.CtStatement;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        FlacocoConfig config = new FlacocoConfig();
// set config options

        Flacoco flacoco = new Flacoco(config);
        FlacocoResult result = flacoco.run();

        Map<Location, CtStatement> mapping = result.getLocationStatementMap();
        Set<TestMethod> failingTestMethods = result.getFailingTests();

        System.out.println("[");

        var susMap = result.getDefaultSuspiciousnessMap();
        var list = new ArrayList<String>();
        for(var entry : susMap.entrySet()){
            var loc = entry.getKey();
            // TODO: find the real file, not just guess
            var a = loc.getClassName().split("\\.");
            var filename = a[a.length - 1] + ".java";
            var filepathOpt = Files.find(
                    Path.of("./"),
                    Integer.MAX_VALUE,
                    (p, basicFileAtrributes) -> p.getFileName().toString().equals(filename)
            ).findAny();
            assert filepathOpt.isPresent();
            var filepath = filepathOpt.get();

            var s = "  {\n" +
                    "    \"info\": {\n" +
                    "      \"sus\": "+entry.getValue().getScore()+"\n" +
                    "    },\n" +
                    "    \"fdts\": {\n" +
                    "      \"type\": \"FDTSImpl\",\n" +
                    "      \"nodes\": [\n" +
                    "        {\n" +
                    "          \"type\": \"AtLine\",\n" +
                    "          \"line\": "+loc.getLineNumber()+",\n" +
                    "          \"path\": \""+filepath+"\"\n" +
                    "        }\n" +
                    "      ],\n" +
                    "      \"edges\": [\n" +
                    "        [\n" +
                    "          null\n" +
                    "        ]\n" +
                    "      ],\n" +
                    "      \"startNodes\": [\n" +
                    "        {\n" +
                    "          \"id\": 0\n" +
                    "        }\n" +
                    "      ]\n" +
                    "    }\n" +
                    "  }";
            list.add(s);
        }

        for(var i = 0; i<list.size(); ++i){
            System.out.println(list.get(i));
            if(i< list.size() - 1)
                System.out.println(",");
        }

        System.out.println("]");
    }

}
