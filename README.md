# FLACOCO FDTS Converter

To build the JAR file click on `Build -> Build Artifacts -> Build`. The resulting JAR with dependencies should be in `./out` directory.

It can be used by executing it in the root project folder of the project you want to test. Needs existing JUnit5 unit tests.
